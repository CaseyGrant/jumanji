#include <iostream> // allows you to use inputs and outputs
#include <string> // allows you to use strings

using namespace std; // allows use of 

int num_roll, num_friends; // allows user input to be stored as an integer
string you, animal, friends, friend_1, friend_2, roll; // allows the user input to be stored as a string
void roll_check();
void friend_check();
bool friend_bool;
bool die_bool;
string h = h;

void main() // runs when the code starts
{
	cout << "Welcome to the Jungle\n\n"; // displays text to screen
	cout << "Enter an animal species: ";
	cin >> animal; // gets the user input and sets adventurers to that input
	do
	{
		cout << "How many friends do you have?: ";
		cin >> friends;
		friend_check();
	} while (friend_bool == true);
	
	cout << "Enter your name: ";
	cin >> you;
	cout << "Okay here's your adventure! \n";

	cout << "It was a sunny day, you where hanging out with your "<< friends << " friends, when one of them said '" << you << ", Lets play Jumanji!' \n";
	cout << "You, having nothing better to do, agreed. \n";

	do
	{
		cout << "You roll a 6 sided die: ";
		cin >> roll;
		roll_check();
	}while (die_bool == true);
	
	if (num_roll == 1 || num_roll == 6) // checks what was rolled and out puts different things depending on that input
	{
		cout << "You rolled a "<< num_roll << " and it summoned a herd of " << animal << "'s! \n";
	}
	
	if (num_roll == 2 || num_roll == 5)
	{
		cout << "You rolled a " << num_roll << " and it summoned a " << animal << "! \n";
	}
	
	if (num_roll == 3 || num_roll == 4)
	{
		cout << "You rolled a " << num_roll << " and a plane driven by " << animal << "'s crashed into your house! \n";
	}
	
	cout << "Seeing the power of the game you and your friends don't know whether or not to continue... \n";
	cout << "What is one of your friends names? ";
	cin >> friend_1;
	do
	{
		cout << "Then " << friend_1 << " rolls the die and gets a ";
		cin >> roll;

		roll_check();
	} while(die_bool == true);

	if (num_roll == 1 || num_roll == 2)
	{
		cout << friend_1 << " rolled a " << num_roll << " and it called forth a meteor that struck the earth obliterating everything... \n";
	}

	if (num_roll == 5)
	{
		cout << friend_1 << " rolled a " << num_roll << " and it caused a massive tsunami that covered the entire planet... \n";
	}

	if (num_roll == 3 || num_roll == 6)
	{
		cout << friend_1 << " rolled a " << num_roll << " and it started WW3... \n";
	}

	if (num_roll == 4)
	{
		cout << friend_1 << " rolled a " << num_roll << " and it undid your roll! \n";
	}

	cout << "What is another friends name? ";
	cin >> friend_2;
	do
	{
		cout << friend_2 << " decides to roll the die too: ";
		cin >> roll;
		roll_check();
	} while (die_bool == true);

	if (num_roll == 1 || num_roll == 6)
	{
		cout << friend_2 << " rolled a " << num_roll << " and was turned into a monkey! \n";
	}

	if (num_roll == 2 || num_roll == 5)
	{
		cout << friend_2 << " rolled a " << num_roll << " and got trapped inside the board game! \n";
	}

	if (num_roll == 3 || num_roll == 4)
	{
		cout << friend_2 << " rolled a " << num_roll << " and a big game hunter appears in front of you all and starts to hunt you!";
	}

	cout << "That concludes your short adventure... but more adventure awaits! \n";
	cout << "Play through again to see how else that could have ended!";
	system("pause");
}

void roll_check() // a function used to make sure the die is a number between 1 and six (if a letter is the input it is an infinite loop and i'm not sure why)
{
	try
	{
		num_roll = stoi(roll);
		if (num_roll >= 7 || num_roll <= 0)
		{
			roll = h;
			num_roll = stoi(roll);
		}
		else
		{
			die_bool = false;
		}
	}
	catch (...)
	{
		cout << "Input must be a number between 1-6! ";
		die_bool = true;
	}
}

void friend_check()
{
	try
	{
		num_friends = stoi(friends);
		friend_bool = false;
	}
	catch (...)
	{
		friend_bool = true;
		cout << "Input must be a number more than 2! \n";
	}
}